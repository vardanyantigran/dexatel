const vectorName = '_search';
const tableName = 'contacts';
const searchObjects = ['first_name', 'last_name', 'number'];

module.exports = {
  up: (queryInterface) => (
    queryInterface.sequelize.transaction((t) =>
      queryInterface.sequelize.query(`
        ALTER TABLE ${tableName} ADD COLUMN ${vectorName} TSVECTOR;
      `, { transaction: t })
        .then(() =>
          queryInterface.sequelize.query(`
              UPDATE ${tableName} SET ${vectorName} = to_tsvector('english', ${searchObjects.join(" || ' ' || ")});
            `, { transaction: t })
        ).then(() =>
          queryInterface.sequelize.query(`
              CREATE INDEX ${tableName}_search ON ${tableName} USING gin(${vectorName});
            `, { transaction: t })
        ).then(() =>
          queryInterface.sequelize.query(`
              CREATE TRIGGER ${tableName}_vector_update
              BEFORE INSERT OR UPDATE ON ${tableName}
              FOR EACH ROW EXECUTE PROCEDURE tsvector_update_trigger(${vectorName}, 'pg_catalog.english', ${searchObjects.join(', ')});
            `, { transaction: t })
        )
        .error(console.log)
    )
  ),

  down: (queryInterface) => (
    queryInterface.sequelize.transaction((t) =>
      queryInterface.sequelize.query(`
        DROP TRIGGER ${tableName}_vector_update ON ${tableName};
      `, { transaction: t })
        .then(() =>
          queryInterface.sequelize.query(`
              DROP INDEX ${tableName}_search;
            `, { transaction: t })
        ).then(() =>
          queryInterface.sequelize.query(`
              ALTER TABLE ${tableName} DROP COLUMN ${vectorName};
            `, { transaction: t })
        )
    )
  ),
};