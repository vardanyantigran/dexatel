'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('groups', [
      { name: 'Rock' },
      { name: 'Jazz' },
      { name: 'Pop' },
      { name: 'Classic' },
      { name: 'Blues' },
      { name: 'HipHop' },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('groups', null, {});
  }
};