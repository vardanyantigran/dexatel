module.exports = (sequelize, DataTypes) => {
  const group = sequelize.define('group', {
    name: {
      type: DataTypes.STRING,
      primaryKey: true,
      unique: true,
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'groups',
    underscored: true,
    paranoid: true,
    defaultScope: {
      attributes: { exclude: ['updatedAt', 'createdAt', 'deletedAt', 'contactGroup'] },
    },
  });
  return group;
};
