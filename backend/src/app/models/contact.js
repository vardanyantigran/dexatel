module.exports = (sequelize, DataTypes) => {
  const contact = sequelize.define('contact', {
    number: {
      type: DataTypes.STRING,
      primaryKey: true,
      unique: true,
      get() {
        return '+' + this.getDataValue('number');
      }
    },
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'contacts',
    underscored: true,
    paranoid: true,
    defaultScope: {
      attributes: { exclude: ['updatedAt', 'createdAt', 'deletedAt', '_search'] },
    },
    
    // -----> INDEX created with seeder <-----
    
    indexes: [
      {
        name: 'index_first_name',
        concurrently: true,
        using: 'BTREE',
        fields: ['first_name'],
      },
      {
        name: 'index_last_name',
        concurrently: true,
        using: 'BTREE',
        fields: ['last_name'],
      },
      {
        name: 'index_number',
        concurrently: true,
        using: 'BTREE',
        fields: ['number'],
      }
    ],
    
  });
  contact.associate = function associate(models) {
    models.contact.belongsToMany(models.group, { through: 'contactGroup' });
  };
  return contact;
};
