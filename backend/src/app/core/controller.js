/* eslint-disable func-names */
/* eslint-disable prefer-rest-params */
import { validationResult } from 'express-validator/check';

export default function controllerWrapper(fn) {
  return async function (req, res, next) {
    try {
      validationResult(req).throw();
      const data = await fn.apply(this, arguments);
      let status;
      switch (req.method) {
        case 'GET':
        case 'PUT':
        case 'PATCH':
          status = 200;
          break;
        case 'POST':
          status = 201;
          break;
        case 'DELETE':
          status = 204;
          break;
        default:
          status = 204;
      }
      return res.status(status).json({
        status: 'SUCCESS',
        message: '',
        data,
        errors: null,
      });
    } catch (error) {
      return next(error);
    }
  };
}
