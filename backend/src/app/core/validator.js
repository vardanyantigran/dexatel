import models from '../models';

export const isExistByParameter = async (search, by, repository) => {
  const response = await repository.findOne({ where: { [by]: search } });
  return !response;
};

export const isExistIncludingDeleted = async (search, by, repository) => {
  const response = await repository.findOne({ where: { [by]: search }, paranoid: false });
  return !response;
};

export const isResourceExist = async (pk, repository) => {
  const response = await repository.findByPk(pk);
  return !!response;
};

export const isExistByCondition = async (where, repository) => {
  const response = await repository.findOne(where);
  return !!response;
};

export const errorMessage = {
  idDeleted: 'The resource is deleted',
  idNotDeleted: 'The resource is not deleted',
};

export const coreValidator = {
  name: {
    in: ['body'],
    errorMessage: errorMessage.name,
    isEmpty: { negated: true },
  },
  offset: {
    in: ['query'],
    isEmpty: { negated: true },
    errorMessage: errorMessage.required,
    isInt: {
      options: { min: 0 },
      errorMessage: errorMessage.offset,
    },
  },
  limit: {
    in: ['query'],
    isEmpty: { negated: true },
    errorMessage: errorMessage.required,
    isInt: {
      options: { min: 0 },
      errorMessage: errorMessage.limit,
    },
  },
  queryString: {
    in: ['query'],
    errorMessage: errorMessage.required,
    matches: {
      options: [/^[a-zA-Z,]{1,}$/],
      errorMessage: errorMessage.queryString,
    },
  },
  tags: {
    in: ['body'],
    isArray: { errorMessage: errorMessage.array },
    errorMessage: errorMessage.tags,
    isEmpty: { negated: true },
    custom: {
      options: (a) => a.every((item) => {
        if (!item) return false;
        return true;
      }),
    },
  },
  integer: {
    in: ['body'],
    isEmpty: { negated: true },
    isInt: {
      errorMessage: errorMessage.integer,
    },
  },
  number: {
    in: ['body'],
    isEmpty: { negated: true },
    isNumeric: {
      errorMessage: errorMessage.number,
    },
  },
  phone: {
    in: ['body'],
    errorMessage: errorMessage.phone,
    isEmpty: { negated: true },
    matches: {
      options: [/^[+]?[0-9]{9,20}$/],
      errorMessage: errorMessage.phoneRegex,
    },
  },
  
  contactId: {
    in: ['params'],
    errorMessage: (val) => `The contact with ID: ${val} does not exist`,
    custom: { options: (id) => isResourceExist(id, models.contact) },
  },
};
