import coreRepository from '../../core/repository';

const repository = Object.create(coreRepository);

export default Object.assign(repository, {
  modelName: 'contact',
  bulkInsert(data) {
    return repository.models.contact.bulkCreate(
      data, 
      {
        validate: true,
        returning: true,
        ignoreDuplicates: true,
        include: [{
          model: repository.models.group,
          validate: true,
          returning: true,
          ignoreDuplicates: true,
        }]
      }
    )
  }
});
