import * as controller from './controller';
import validator from './validator';

export default (router) => {
  // ----------------------------------------------------------------------------------------------
  router.get('/', validator.getAll, controller.getAll);
  // ----------------------------------------------------------------------------------------------
  router.get('/:id', validator.isExist, controller.get);
  // ----------------------------------------------------------------------------------------------
  router.post('/', validator.store, controller.store);
  // ----------------------------------------------------------------------------------------------
  router.put('/:id', validator.isExist, validator.upsert, controller.upsert);
  // ----------------------------------------------------------------------------------------------
  router.patch('/:id', validator.isExist, validator.update, controller.update);
  // ----------------------------------------------------------------------------------------------
  router.delete('/', controller.remove);
  // ----------------------------------------------------------------------------------------------
  router.post('/group/:id', controller.setGroups);
  // ----------------------------------------------------------------------------------------------
  router.post('/insert', controller.bulkInsert);
};
