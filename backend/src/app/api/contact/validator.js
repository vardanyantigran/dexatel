import { checkSchema } from 'express-validator/check';
import { coreValidator } from '../../core/validator';

const validator = Object.create(coreValidator);

export default Object.assign(validator, {
  getAll: checkSchema({
    offset: { ...coreValidator.offset, optional: true },
    limit: { ...coreValidator.limit, optional: true },
  }),

  isExist: checkSchema({
    id: coreValidator.contactId,
  }),

  store: checkSchema({
  }),

  upsert: checkSchema({
  }),

  update: checkSchema({
  }),
});
