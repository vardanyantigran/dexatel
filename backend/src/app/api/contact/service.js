import fs from 'fs';
import path from 'path';
import P from 'bluebird';
import csv from 'csv-parser';

import coreService from '../../core/service';
import repository from './repository';
import { ServiceError } from '../../utils/error_handler';

const BlueBird = P.promisifyAll(fs);
const service = Object.create(coreService);

export default Object.assign(service, {
  /**
   *  Get All available resources
   *  @return {Array} array of resource ORM objects
   */
  async getAll(query) {
    try {
      let { offset = 0, limit = 10, q } = query;
      const options = {
        offset,
        limit,
        include: ['groups']
      }

      
      if(q) {
        q = q.replace('+', '').trim();
        const { Op, sequelize } = repository.models; 
        options.where = { 
          [Op.or]: [
            { firstName: sequelize.where(sequelize.fn('LOWER', sequelize.col('first_name')), 'LIKE', q.toLowerCase() + '%')},
            { lastName: sequelize.where(sequelize.fn('LOWER', sequelize.col('last_name')), 'LIKE', q.toLowerCase() + '%')},
            { number: sequelize.where(sequelize.fn('LOWER', sequelize.col('number')), 'LIKE', q.toLowerCase() + '%')},
          ]
        };
      }
      const response = await repository.findAndCountAll(options);
      return {
        pages: Math.ceil(response.count / limit),
        ...response,
      }
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Get One available resources
   *  @param {Number} pk : resource ID
   *  @return {Object} resource ORM object
   */
  getByPk(pk) {
    try {
      return repository.findByPk(pk);
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Create resources
   *  @param {Object} data : resource data object
   *  @return {Object} resource ORM object
   */
  async store(data) {
    try {
      const contact = await repository.create({
        ...this.sanitizeData(data),
      });
      if(data.groups && data.groups.length) contact.setGroups(data.groups);
      return contact;
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  async bulkInsert(file) {
    const filePath = path.join(global.uploadFolder, `${file.name}_${Date.now()}`);
    const data = [];
    fs.writeFileSync(filePath, file.data);
    const promise = await new Promise((resolve, reject) => 
      fs.createReadStream(filePath)
        .pipe(csv())
        .on('error', error => reject(error))
        .on('data', row => {
          data.push({
            firstName: row.first_name.trim(),
            lastName: row.last_name.trim(),
            number: this.sanitizeNumber(row.number),
            groups: row.group.split('|').filter(i => !!i).map(i => ({ name: i.trim() }))
          })
        })
        .on('end', async () => {
          const response = await repository.bulkInsert(data)
          resolve(response.length)
        })
    );
    return { status: promise };
  },

  /**
   *  Update or Create resources
   *  @param {Object} data : resource data object
   *  @param {Object} id : resource ID
   *  @return {Object} resource ORM object
   */
  upsert(data, id) {
    try {
      return repository.upsert({
        ...this.sanitizeData(data),
        id,
      });
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Update resources
   *  @param {Object} data : resource data object
   *  @param {Object} id : resource ID
   *  @return {Object} resource ORM object
   */
  async update(data, id) {
    try {
      const contact = await repository.update(
        this.sanitizeData(data),
        { id },
      );
      if(data.groups && data.groups.length) contact.setGroups(data.groups);
      return contact;
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Remove resources
   *  @param {Object} id : resource ID
   *  @return {Object} resource ORM object
   */
  async remove(id) {
    try {
      const result = await repository.destroy({ number: id });
      return { affectedRows: 'ok' };
    } catch (error) {
      throw new ServiceError();
    }
  },

  async setGroups(id, groupIds) {
    try {
      const contact = await this.getByPk(id);
      contact.setGroups(groupIds);
      return contact;
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  sanitizeData(data) {
    data.number = this.sanitizeNumber(data.number);
    return data;
  },

  sanitizeNumber(number) {
    return number.replaceAll('+', '').replaceAll('-', '').replaceAll(' ', '').trim();
  }
});
