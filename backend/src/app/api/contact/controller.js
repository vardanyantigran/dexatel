import service from './service';
import cw from '../../core/controller';

export const getAll = cw((req) => service.getAll(req.query));

export const get = cw((req) => service.getByPk(req.params.id));

export const store = cw((req) => service.store(req.body));

export const upsert = cw((req) => service.upsert(req.body, req.params.id));

export const update = cw((req) => service.update(req.body, req.params.id));

export const remove = cw((req) => service.remove(req.body.id));

export const setGroups = cw((req) => service.setGroups(req.params.id, req.body.groups));

export const bulkInsert = cw((req) => service.bulkInsert(req.files.file));
