# README #

This README would normally document whatever steps are necessary to get your application up and running.

### How do I get set up? ###

* Build app via docker compose ```docker-compose build```
* Run app via docker compose ```docker-compose up```
* Stop app via docker compose ```docker-compose stop```
* Open app at [http://localhost:3000](http://localhost:3000)
* BackEnd running at [http://localhost:8000](http://localhost:8000)
* DB running at [http://localhost:5432](http://localhost:5432)
* BackEnd Postman Collection in project root dir