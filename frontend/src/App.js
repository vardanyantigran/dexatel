import EditableTable from "./components/Table";

function App() {
  return (
    <div className="App">
      <EditableTable />
    </div>
  );
}

export default App;
