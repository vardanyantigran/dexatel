/* eslint-disable no-template-curly-in-string */
import React, { useContext, useEffect, useState } from 'react';

import { Upload, Form, Input, Button } from 'antd';

// import { CustomFormContext } from '@caimankit/Form/context';

const apiUrl = 'http://localhost:8000';
const Uploader = ({ form, name, buttonText = 'Upload', singleUpload = false, reloadPage }) => {
  const [fileList, setFileList] = useState([]);
//   const formObserver = useContext(CustomFormContext)?.formObserver;

  const onFileChange = ({ file, fileList, event }) => {
    singleUpload ? setFileList([file]) : setFileList(fileList);
  };

//   useEffect(() => {
//     formObserver?.subscribe('onReset', name, () => setFileList([]));
//     return () => formObserver?.unsubscribe('onReset', name);
//   }, [formObserver, name]);

  return (
    <>
      <Form.Item name={name} noStyle>
        <Input type='hidden' />
      </Form.Item>
      <Form.Item>
        <Upload
          listType='picture'
          fileList={fileList}
          onChange={onFileChange}
          multiple={!singleUpload}
          onRemove={removedFile => {
            if (singleUpload) {
              setFileList([]);
              form.setFieldsValue({ [name]: null });
            } else {
              const removedIndex = fileList.findIndex(file => file.uid === removedFile.uid);
              const fileIds = form.getFieldValue(name);
              fileIds.splice(removedIndex, 1);
              form.setFieldsValue({ [name]: [...fileIds] });
              fileList.splice(removedIndex, 1);
              setFileList([...fileList]);
            }

            return false; // --- to cancel native onChange
          }}
          customRequest={async ({ file, onSuccess }) => {

            const formData = new FormData();
            formData.append('file', file);
            fetch(`${apiUrl}/contact/insert`, { method: 'POST', body: formData })
            .then(response => response.json())
            .then(data => {
              onSuccess();
              reloadPage();
            });
          }}
        >
          <Button type='primary' htmlType='button'>
            {buttonText}
          </Button>
        </Upload>
      </Form.Item>
    </>
  );
};

export default Uploader;
