/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useEffect } from 'react';
import {
  Tag,
  Row,
  Form,
  Space,
  Table,
  Input,
  Button,
  Typography,
  Popconfirm,
  InputNumber,
  Select,
} from 'antd';
import Uploader from './Uploader';

const { Search } = Input;
const apiUrl = 'http://localhost:8000';
const originData = [];

for (let i = 0; i < 2; i++) {
  originData.push({
    key: i.toString(),
    number: 94323232,
    firstName: `Tigran ${i}`,
    lastName: `Vardanyan ${i}`,
    tags: [
      { tag: `Tag ${i}`, key: `tag_${i}` },
      { tag: `Tag ${i + 1}`, key: `tag_${i + 1}` },
    ],
  });
}

const EditableCell = ({
  index,
  title,
  record,
  editing,
  children,
  dataIndex,
  inputType,
  ...restProps
}) => {
  const inputNode = inputType === 'number' ? <InputNumber /> : <Input />;

  // console.log(record);
  return (
    <td {...restProps}>
      {editing ? (
        dataIndex === 'tags' ? (
          <>
            {record.tags.map((item, index) => (
              <Form.Item
                key={index}
                name={[dataIndex]}
                style={{
                  margin: 0,
                }}
                rules={[
                  {
                    required: true,
                    message: `Please Input ${title}!`,
                  },
                ]}
              >
                {/* don`t work for tags structure bag maybe */}
                {/* <Select>
                  <Select.Option value='1'>Example 1</Select.Option>
                  <Select.Option value='2'>Example 2</Select.Option>
                </Select> */}
              

              </Form.Item>
            ))}
          </>
        ) : (
          <Form.Item
            name={dataIndex}
            style={{
              margin: 0,
            }}
            rules={[
              {
                required: true,
                message: `Please Input ${title}!`,
              },
            ]}
          >
            {inputNode}
          </Form.Item>
        )
      ) : (
        children
      )}
    </td>
  );
};

const EditableTable = () => {
  const [form] = Form.useForm();
  const [data, setData] = useState([]);
  const [totalCount, setTotalCount] = useState(1);
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(10);
  const [editingKey, setEditingKey] = useState('');
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const findData = ({ offset, limit, q }) =>
    fetch(`${apiUrl}/contact?${q ? `q=${q}&` : ''}${limit ? `offset=${offset}&limit=${limit}` : ''}`)
      .then(response => response.json())
      .then(data => {
        setTotalCount(data.data.count);
        const rows = data.data.rows.map(it => {
          const { number, firstName, lastName, groups } = it;

          return {
            key: number.toString(),
            number,
            firstName,
            lastName,
            tags: groups.map((item, index) => {
              return {
                tag: item.name,
                key: index,
              };
            }),
          };
        });
        console.log(rows);
        setData(rows);
      });
  const onSearch = value => {
    findData({ offset: 0, limit, q: value })
  };
  const deleteData = ({ offset, limit }) =>
    fetch(`${apiUrl}/contact?${limit ? `offset=${offset}&limit=${limit}` : ''}`)
      .then(response => response.json())
      .then(data => {
        setTotalCount(data.data.count);
        const rows = data.data.rows.map(it => {
          const { number, firstName, lastName, groups } = it;

          return {
            key: number.toString(),
            number,
            firstName,
            lastName,
            tags: groups.map((item, index) => {
              return {
                tag: item.name,
                key: index,
              };
            }),
          };
        });
        console.log(rows);
        setData(rows);
      });
  const isEditing = record => record.key === editingKey;

  const edit = record => {
    form.setFieldsValue({
      number: '',
      lastName: '',
      firstName: '',
      ...record,
    });
    setEditingKey(record.key);
  };

  useEffect(() => {
    findData({ offset: 0, limit: 20 });
  }, []);
  useEffect(() => {
    findData({ offset: (page - 1) * limit, limit });
  }, [page, limit]);

  const reloadPage = () => {
    findData({ offset: (page - 1) * limit, limit });
  }
  const remove = rowKey => {
    const removingRows = typeof rowKey === 'string' ? [rowKey] : selectedRowKeys;
    if (removingRows.length) {
      const newData = data.filter(item => !removingRows.includes(item.key));
      setData(newData);
    }
  };

  const cancel = () => {
    setEditingKey('');
  };

  const getMore = (page, pageSize) => {
    setPage(page);
    setLimit(pageSize);
  };

  const save = async key => {
    try {
      const row = await form.validateFields();
      const newData = [...data];
      const index = newData.findIndex(item => key === item.key);

      if (index > -1) {
        const item = newData[index];
        newData.splice(index, 1, { ...item, ...row });
        setData(newData);
        setEditingKey('');
      } else {
        newData.push(row);
        setData(newData);
        setEditingKey('');
      }
    } catch (errInfo) {
      console.log('Validate Failed:', errInfo);
    }
  };

  const columns = [
    {
      title: 'First Name',
      dataIndex: 'firstName',
      width: '25%',
      editable: true,
    },
    {
      dataIndex: 'lastName',
      title: 'Last Name',
      width: '15%',
      editable: true,
    },
    {
      dataIndex: 'number',
      title: 'Number',
      width: '15%',
      editable: true,
    },
    {
      dataIndex: 'tags',
      title: 'Groups',
      width: '25%',
      editable: true,
      render: tags =>
        tags.map(el => (
          <Tag color='blue' key={el.key}>
            {el.tag}
          </Tag>
        )),
    },
    {
      title: 'Operation',
      dataIndex: 'operation',
      render: (_, record) => {
        const editable = isEditing(record);

        return (
          <>
            {editable ? (
              <span>
                <a
                  // eslint-disable-next-line no-script-url
                  href='javascript:;'
                  onClick={() => save(record.key)}
                  style={{
                    marginRight: 8,
                  }}
                >
                  Save
                </a>
                <Popconfirm title='Sure to cancel?' onConfirm={cancel}>
                  <a>Cancel</a>
                </Popconfirm>
              </span>
            ) : (
              <Space size={32}>
                <Typography.Link
                  type='success'
                  disabled={editingKey !== ''}
                  onClick={() => edit(record)}
                >
                  Edit
                </Typography.Link>
                <Typography.Link
                  type='danger'
                  disabled={editingKey !== ''}
                  onClick={() => remove(record.key)}
                >
                  Delete
                </Typography.Link>
              </Space>
            )}
          </>
        );
      },
    },
  ];

  const mergedColumns = columns.map(col => {
    if (!col.editable) {
      return col;
    }

    return {
      ...col,
      onCell: record => ({
        record,
        title: col.title,
        editing: isEditing(record),
        dataIndex: col.dataIndex,
        inputType: col.dataIndex === 'number' ? 'number' : 'text',
      }),
    };
  });

  const onSelectChange = selectedRowKeys => {
    setSelectedRowKeys(selectedRowKeys);
  };
  return (
    <Form form={form} component={false}>
      <Row justify='space-between'>
        <Button
          onClick={remove}
          type='primary'
          style={{
            marginBottom: 16,
          }}
        >
          Delete selected
        </Button>
        <Search placeholder="input search text" onSearch={onSearch} enterButton style={{ width:'50%'}} />

        <Uploader form={form} singleUpload reloadPage={reloadPage} />
      </Row>
      <Table
        components={{
          body: {
            cell: EditableCell,
          },
        }}
        bordered
        dataSource={data}
        columns={mergedColumns}
        rowClassName='editable-row'
        rowSelection={{ selectedRowKeys, onChange: onSelectChange }}
        pagination={{
          total: totalCount,
          onChange: getMore,
          onShowSizeChange: getMore,
        }}
      />
    </Form>
  );
};

export default EditableTable;
